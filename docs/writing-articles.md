## Front matter

Each blog post requires a front matter at the beginning of the file. It looks
like this:

```yaml
+++
title = "Blog post title"
date = "2020-09-06"
[extra]
author = "Alice Andersson"
+++
```

These are the required fields:

```
+--------------+-----------------------------------------+----------------+
| Field        | Description                       | Example(-s)          |
+--------------+-----------------------------------------+----------------+
| title        | The title of the post.            | Blog post title      |
| date         | The date this post was published. | 2020-09-06           |
| extra.author | The author of this post.          | 1) Alice Andersson   |
|              |                                   | 2) The maintainers   |
|              |                                   | 3) A group of people |
|              |                                   |    (via Mikael J.)   |
+--------------+-----------------------------------------+----------------+
```

(The date can also be set in the filename. If the file is named e.g.
`2020-09-06-blog-post-title.md`, the date is set to `2020-09-06` if it is
missing from the front matter.)

The following fields can additionally be set. They are not required.

```
+-----------------+----------------------------------+-------------------------+
| Field           | Description                      | Example(-s)             |
+-----------------+----------------------------------+-------------------------+
| extra.updated   | When this post was last updated. | 2022-08-15              |
| extra.toc       | Whether to render a table of     | true                    |
|                 | contents before the post.        |                         |
| taxonomies.tags | Tags to group this post with.    | tags = ["tag1", "tag2"] |
+-----------------+----------------------------------+-------------------------+
```

Do keep in mind that no taxonomies have been configured so you would need to
edit some templates and make sure it looks nice before using them.

## Content

The content is written using [CommonMark](https://commonmark.org/).

## Merge request

After you've written your post (draft or finished), submit a merge request to
the [main repository](https://gitlab.com/spade-lang/blog.spade-lang.org/). After
merging, the site is built and published by CI.
