+++
sort_by = "date"
insert_anchor_links = "right"
template = "section.html"
[extra.show_posts]
enable = true
dates = true
authors = true
+++

Welcome to the Surfer blog! Surfer is an extensible and snappy waveform viewer. You can try it in the browser at [https://app.surfer-project.org](https://app.surfer-project.org) For download links and more information about the project, see the [main website](https://surfer-project.org).

[Blog RSS feed](/atom.xml).
